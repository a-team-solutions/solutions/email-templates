# Email Templates

Framework that generates responsive HTML templates with MJML and Nunjucks.
Based on [@mvsde/mailbox →](https://www.npmjs.com/package/@mvsde/mailbox)

**[Choosing a Responsive Email Framework: MJML vs. Foundation for Emails →](https://css-tricks.com/choosing-a-responsive-email-framework%E2%80%8Amjml-vs-foundation-for-emails/)**

## MJML - templates and tooling

[MJML →](https://mjml.io/) framework that makes responsive email easy

[Responsive Email Templates →](https://mjml.io/templates)

[MJML online template editor →](https://mjml.io/try-it-live/templates/basic)

[Online template editor →](https://www.mailjet.com/demo/)

## HTML email check

<https://www.htmlemailcheck.com/check/>

## Project documentation

[Mailbox documentation →](https://github.com/mvsde/mailbox#readme)

## Installation

1. Install [Node.js](https://nodejs.org)
2. Install dependencies:

```bash
npm install
```

## Development

```bash
npm run dev
```

## Production

```bash
npm run build
```

## Test email

```bash
npm run test -- --to info@example.com
```

## Help

```bash
npx @mvsde/mailbox help
```
